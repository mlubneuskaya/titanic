import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    df['Title'] = df['Name'].apply(lambda x: x.split(', ')[1].strip().split(' ')[0])
    df_short = df[df['Title'].isin(['Mr.', 'Miss.', 'Mrs.'])]
    missing = df_short.groupby('Title')['Age'].apply(lambda x: x.isnull().sum())
    medians = df_short.groupby('Title')['Age'].median()

    result = [('Mr.', missing['Mr.'], round(medians['Mr.'])),
              ('Mrs.', missing['Mrs.'], round(medians['Mrs.'])),
              ('Miss.', missing['Miss.'], round(medians['Miss.']))]
    return result


print(get_filled())
